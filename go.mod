module gitlab.com/batas/s3-proxy

go 1.16

require (
	github.com/aws/aws-sdk-go v1.40.8
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-openapi/swag v0.19.15
	github.com/h2non/bimg v1.1.5
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
)
