# AWS S3 Proxy v2.0
# docker run -d -p 8080:80 -e AWS_REGION -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e AWS_S3_BUCKET pottava/s3-proxy

FROM golang:1.16.2-alpine3.13 AS builder
RUN apk --no-cache add gcc musl-dev git vips-dev
WORKDIR /code
COPY go.* /code/
RUN go mod download
RUN go mod verify

COPY . /code
ENV APP_VERSION=v2.0.0
RUN today=$(date +%Y-%m-%d --utc) \
    && CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build \
    -ldflags '-s -w -X main.ver=${APP_VERSION} -X main.commit=master -X main.date=${today}' \
    -o /app

FROM alpine:3.13
RUN apk --no-cache add ca-certificates vips
#COPY --from=libs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app /aws-s3-proxy
ENTRYPOINT ["/aws-s3-proxy"]
