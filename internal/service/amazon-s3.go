package service

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"gitlab.com/batas/s3-proxy/internal/config"
	"io"
)

// S3get returns a specified object from Amazon S3
func (c client) S3get(bucket, key string, rangeHeader *string) (*s3.GetObjectOutput, error) {
	req := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Range:  rangeHeader,
	}
	return s3.New(c.Session).GetObjectWithContext(c.Context, req)
}

// S3listObjects returns a list of s3 objects
func (c client) S3listObjects(bucket, prefix string) (*s3.ListObjectsOutput, error) {
	req := &s3.ListObjectsInput{
		Bucket:    aws.String(bucket),
		Prefix:    aws.String(prefix),
		Delimiter: aws.String("/"),
	}
	// List 1000 records
	if !config.Config.AllPagesInDir {
		return s3.New(c.Session).ListObjectsWithContext(c.Context, req)
	}
	// List all objects with pagenation
	result := &s3.ListObjectsOutput{
		CommonPrefixes: []*s3.CommonPrefix{},
		Contents:       []*s3.Object{},
		Prefix:         aws.String(prefix),
	}
	err := s3.New(c.Session).ListObjectsPagesWithContext(c.Context, req,
		func(page *s3.ListObjectsOutput, lastPage bool) bool {
			result.CommonPrefixes = append(result.CommonPrefixes, page.CommonPrefixes...)
			result.Contents = append(result.Contents, page.Contents...)
			return len(page.Contents) == 1000
		})
	return result, err
}

func (c client) S3put(bucket *string, key *string, obj io.ReadSeeker) (*s3.GetObjectOutput, error) {
	input := s3.PutObjectInput{
		ACL:                       nil,
		Body:                      obj,
		Bucket:                    bucket,
		CacheControl:              nil,
		ContentDisposition:        nil,
		ContentEncoding:           nil,
		ContentLanguage:           nil,
		ContentLength:             nil,
		ContentMD5:                nil,
		ContentType:               nil,
		ExpectedBucketOwner:       nil,
		Expires:                   nil,
		GrantFullControl:          nil,
		GrantRead:                 nil,
		GrantReadACP:              nil,
		GrantWriteACP:             nil,
		Key:                       key,
		Metadata:                  nil,
		ObjectLockLegalHoldStatus: nil,
		ObjectLockMode:            nil,
		ObjectLockRetainUntilDate: nil,
		RequestPayer:              nil,
		SSECustomerAlgorithm:      nil,
		SSECustomerKey:            nil,
		SSECustomerKeyMD5:         nil,
		SSEKMSEncryptionContext:   nil,
		SSEKMSKeyId:               nil,
		ServerSideEncryption:      nil,
		StorageClass:              nil,
		Tagging:                   nil,
		WebsiteRedirectLocation:   nil,
	}

	_, err := s3.New(c.Session).PutObject(&input)
	if err != err {
		return nil, err
	}

	return c.S3get(*bucket, *key, nil)
}

func (c client) S3Upload(bucket *string, key *string, obj io.Reader) error {
	uploadInput := s3manager.UploadInput{
		ACL:                       nil,
		Body:                      obj,
		Bucket:                    bucket,
		BucketKeyEnabled:          nil,
		CacheControl:              nil,
		ContentDisposition:        nil,
		ContentEncoding:           nil,
		ContentLanguage:           nil,
		ContentMD5:                nil,
		ContentType:               nil,
		ExpectedBucketOwner:       nil,
		Expires:                   nil,
		GrantFullControl:          nil,
		GrantRead:                 nil,
		GrantReadACP:              nil,
		GrantWriteACP:             nil,
		Key:                       key,
		Metadata:                  nil,
		ObjectLockLegalHoldStatus: nil,
		ObjectLockMode:            nil,
		ObjectLockRetainUntilDate: nil,
		RequestPayer:              nil,
		SSECustomerAlgorithm:      nil,
		SSECustomerKey:            nil,
		SSECustomerKeyMD5:         nil,
		SSEKMSEncryptionContext:   nil,
		SSEKMSKeyId:               nil,
		ServerSideEncryption:      nil,
		StorageClass:              nil,
		Tagging:                   nil,
		WebsiteRedirectLocation:   nil,
	}

	uploader := s3manager.NewUploader(c)
	_, err := uploader.Upload(&uploadInput)

	if err != err {
		return err
	}
	return nil
}
