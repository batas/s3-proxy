package api

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/go-openapi/swag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/batas/s3-proxy/internal/config"
	"gitlab.com/batas/s3-proxy/internal/controllers"
	"gitlab.com/batas/s3-proxy/internal/processors"
	"gitlab.com/batas/s3-proxy/internal/service"
	"io"
	"net/http"
	"regexp"
	"strings"
	"time"
)

func generateSha(path string) string {
	h := sha256.New()
	return fmt.Sprintf("%x", h.Sum([]byte(path)))
}


func isImage(path string) bool {
	imageExtensions := []string{".jpg", ".jpeg", ".png"}
	for _, ext := range imageExtensions {
		if strings.HasSuffix(path, ext){
			return true
		}
	}
	return false
}

type ProxyApi struct {
}

func (a *ProxyApi) Wrapper(w http.ResponseWriter, r *http.Request) {
	c := config.Config

	start := time.Now()

	hash := generateSha(r.RequestURI)

	var rangeHeader *string
	if candidate := r.Header.Get("Range"); !swag.IsZero(candidate) {
		rangeHeader = aws.String(candidate)
	}

	path := r.URL.Path
	if len(c.StripPath) > 0 {
		path = strings.TrimPrefix(path, c.StripPath)
	}

	if path == "/" {
		_, _ = w.Write([]byte("Hello my friend"))
		w.WriteHeader(200)
		return
	}

	originalPath := path

	optionsRegexp, err := regexp.Compile("([a-z]+)=([^-]+?)/")
	if err != nil {
		log.Errorf("Failed compile regex: %s", err.Error())
		return
	}
	optionsList := optionsRegexp.FindAllStringSubmatch(path, -1)
	options := make(map[string]string)
	for _, option := range optionsList {
		path = strings.Replace(path, option[0], "", 1)
		options[option[1]] = option[2]
	}

	client := service.NewClient(r.Context(), aws.String(config.Config.AwsRegion))

	cachedKey := c.S3KeyPrefix + "tmp/" + hash
	cached, err := client.S3get(c.S3Bucket, cachedKey, rangeHeader)
	if err == nil {
		if *cached.ContentLength > 0 {
			_, _ = io.Copy(w, cached.Body)
			log.WithField("time", time.Since(start)).WithField("url", originalPath).Debug("[time][cache] response")
			return
		}
	}

	startS3 := time.Now()
	key := c.S3KeyPrefix + path
	obj, err := client.S3get(c.S3Bucket, key, rangeHeader)
	log.WithField("time", time.Since(startS3)).Debug("[time] s3 download")

	if err != nil && c.FallbackUrl != "" {
		code, _ := controllers.S3ErrorToHttp(err)
		if code == 404 {
			log.WithField("url", originalPath).Debug("trying fallback")
			obj, err = processors.FallbackProcessor(r, client, c.S3Bucket, err)
		}
	}
	if err != nil {
		log.WithField("key", key).Errorf("failed fetch: %s", err.Error())
		code, _ := controllers.S3ErrorToHttp(err)
		w.WriteHeader(code)
		return
	}

	if strings.HasPrefix(*obj.ContentType, "image/") || isImage(path) {
		err := processors.ImageProcessor(r, obj, options)
		if err != nil {
			w.WriteHeader(500)
			return
		}
	}

	buf := new(bytes.Buffer)
	tee := io.TeeReader(obj.Body, buf)
	_, _ = io.Copy(w, tee)

	go func() {
		err := client.S3Upload(&c.S3Bucket, &cachedKey, buf)
		if err != nil {
			log.WithField("key", cachedKey).Errorf("[cache] failed to upload cache %s", err.Error())
		} else {
			log.WithField("key", cachedKey).Debug("[cache] cache uploaded successfully")
		}
	}()

	log.WithField("time", time.Since(start)).WithField("url", originalPath).Debug("[time] response")
}
