package processors

import (
	"bytes"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/h2non/bimg"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"strconv"
	"time"
)

func ImageProcessor(r *http.Request, obj *s3.GetObjectOutput, options map[string]string) error {
	start := time.Now()

	toWebp := options["f"] == "webp"
	toWidth, err := strconv.Atoi(options["w"])
	if err != nil {
		toWidth = 0
	}

	// TODO add allowed widths

	buf := new(bytes.Buffer)
	_, _ = io.Copy(buf, obj.Body)
	orgSize := buf.Len()

	image := bimg.NewImage(buf.Bytes())

	contentType := ""

	if options["c"] != "" {
		size, _ := image.Size()
		newWidth := 0
		newHeight := 0
		switch crop := options["c"]; crop {
		case "square":
			newWidth = int(math.Min(float64(size.Width), float64(size.Height)))
			newHeight = newWidth
		case "4:3":
			newWidth = size.Width
			newHeight = int(float64(size.Width)*.75)
		}

		if newWidth > 0 && newHeight > 0 {
			_, err = image.Crop(newWidth, newHeight, bimg.GravityCentre)

			if err != nil {
				log.Errorf("[image] error image processing: %s", err)
				return err
			}
			orgSize = len(image.Image())
			log.Debugf("[image] crop from %dx%d to %dx%d", size.Width, size.Height, newWidth, newHeight)
		}
	}

	size, err := image.Size()
	if err != nil {
		log.Errorf("[image] failed getting image size: %s", err)
		return err
	}

	maxSize := int(math.Min(2500.0, float64(size.Width)))
	_toWidth := float64(maxSize)

	if size.Width > maxSize || size.Height > maxSize {
		_toWidth = float64(maxSize)
		if size.Width < maxSize {
			_toWidth = float64(size.Width) / float64(size.Height) * float64(maxSize)
		}
	} else {
		_toWidth = float64(maxSize)
	}

	if toWidth > 0 {
		toWidth = int(math.Min(float64(toWidth), _toWidth))
	} else if _toWidth > 0 {
		toWidth = int(_toWidth)
	}

	if toWidth > 0 {
		_, err = image.Resize(toWidth, 0)

		if err != nil {
			log.Errorf("[image] error image processing: %s", err)
			return err
		}

		orgSize = len(image.Image())
		log.Debugf("[image] resize from %dx%d to %d", size.Width, size.Height, toWidth)
	}

	if toWebp {
		_, err := image.Process(bimg.Options{
			Type: bimg.WEBP,
		})

		if err != nil {
			log.Errorf("[image] error image processing: %s", err)
			return err
		}

		contentType = "image/webp"
		log.Printf("[image] reduce size from %d to %d (%.0f%%)",
			orgSize, len(image.Image()), float64(len(image.Image()))/float64(buf.Len())*100)
	}

	if contentType != "" {
		obj.ContentType = &contentType
	}

	_, err = image.Process(bimg.Options{StripMetadata: true})
	if err != nil {
		log.Errorf("[image] failed strip metadata: %s", err)
		return err
	}

	contentLength := int64(len(image.Image()))
	obj.ContentLength = &contentLength
	obj.Body = ioutil.NopCloser(bytes.NewReader(image.Image()))

	log.Printf("[time] image processing: %s", time.Since(start))
	return nil
}
