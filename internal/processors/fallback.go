package processors

import (
	"bytes"
	"github.com/aws/aws-sdk-go/service/s3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/batas/s3-proxy/internal/config"
	"gitlab.com/batas/s3-proxy/internal/service"
	"io"
	"net/http"
)

func FallbackProcessor(r *http.Request, client service.AWS, bucket string, orgErr error) (*s3.GetObjectOutput, error) {
	fallbackUrl := config.Config.FallbackUrl
	result, err := http.Get(fallbackUrl)
	if err != nil {
		log.WithField("url", fallbackUrl).WithField("error", err.Error()).Errorf("[fallback] failed to fetching url")
		return nil, orgErr
	}

	if result.StatusCode >= 300 {
		log.Errorf("[fallback] cannot fetch resource from fallback (status: %s)", result.Status)
		return nil, orgErr
	}

	content, err := io.ReadAll(result.Body)

	if err != nil {
		log.WithField("url", fallbackUrl).WithField("error", err.Error()).Errorf("[fallback] failed to read url")
		return nil, orgErr
	}

	obj, err := client.S3put(&bucket, &r.RequestURI, bytes.NewReader(content))
	if err != nil {
		log.WithField("url", fallbackUrl).WithField("error", err.Error()).Errorf("[fallback] cannot save fallback to s3")
		return nil, orgErr
	}

	return obj, nil
}
